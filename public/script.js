const langButtons = document.querySelectorAll("[data-btn]");
const allLangs = ["bg", "en", "ru"];
const currentPathName = window.location.pathname;
let currentLang =
	localStorage.getItem("language") || checkBrowserLang() || "bg";
let currentTexts = {};


const homeTexts = {
	"home_page-title": {
		bg: "Djanny Restaurant - Най-доброто място в Слънчев бряг.",
		en: "Djanny Restaurant - Sunny Beach best place to eat.",
		ru: "Djanny Restaurant - Лучшее место чтобы поесть.",
	},
	"header-1": {
		bg: "ОПИТАЙТЕ НАШИТЕ ЯСТИЯ",
		en: "TASTE OUR DISHES",
		ru: "ПОПРОБУЙТЕ НАШИ БЛЮДА",
	},
	"header-2": {
		bg: "Хората, храната и идеалното местоположение правят Джанни превъсходно място за добри приятели и семейство, където могат да се съберат и да прекарат времето си страхотно.",
		en: "The people, the food and the perfect location make Djanny a great place for friends and family to get together and have a great time.",
		ru: "Люди, еда и идеальное расположение делают Джанни прекрасным местом для друзей и семьи, где можно собраться вместе и отлично провести время.",
	},
	"book_button-1": {
		bg: "РЕЗЕРВИРАЙТЕ МАСА",
		en: "BOOK TABLE",
		ru: "ЗАБРОНИРОВАТЬ СТОЛ",
	},
	"about-1": {
		bg: "ЗА НАС",
		en: "ABOUT US",
		ru: "О НАС",
	},
	"about-2": {
		ru: "Добро пожаловать в наш уютный ресторан, который с гордостью радует гостей уже 25 лет! Наше заведение — не просто ресторан, это место, где каждый визит становится встречей с вкусом, атмосферой и гостеприимством. ",
		en: "Welcome to our cozy restaurant, which has been proudly delighting guests for 25 years! Our establishment is not just a restaurant, it is a place where every visit becomes an encounter with taste, atmosphere and hospitality.",
		bg: "Заповядайте в нашия уютен ресторант, който вече 25 години с гордост радва гостите! Нашето заведение не е просто ресторант, то е място, където всяко посещение се превръща в среща с вкус, атмосфера и гостоприемство.",
	},
	"about-3": {
		ru: "Каждое блюдо приготовлено с любовью и профессионализмом наших поваров. Мы гордимся тем, что используем только самые свежие и качественные ингредиенты, чтобы каждый гость мог насладиться изысканными блюдами нашего меню. Но наш ресторан — это не только о еде.",
		en: "Each dish is prepared with love and professionalism by our chefs. We pride ourselves on using only the freshest, highest quality ingredients to ensure every guest can enjoy our delicious menu items. But our restaurant is not only about food.",
		bg: "Всяко ястие е приготвено с любов и професионализъм от нашите готвачи. Ние се гордеем с това, че използваме само най-пресните и висококачествени съставки, за да гарантираме, че всеки гост може да се наслади на нашите вкусни елементи от менюто. Но нашият ресторант не е само храна.",
	},
	"menu-1": {
		bg: "МЕНЮ",
		en: "MENU",
		ru: "МЕНЮ",
	},
	"menu_name-1": {
		bg: "Риба и морски дарове",
		en: "Fish and seafood",
		ru: "Рыба и морепродукты",
	},
	"menu_name-2": {
		bg: "Месо и Пиле",
		en: "Meat and Chicken",
		ru: "Мясо и Курица",
	},
	"menu_name-3": {
		bg: "Паста и Ризото",
		en: "Pasta and Rissotto",
		ru: "Паста и Ризотто",
	},
	"menu_name-4": {
		bg: "Салати",
		en: "Salads",
		ru: "Салаты",
	},
	"menu_name-5": {
		bg: "Пица",
		en: "Pizza",
		ru: "Пицца",
	},
	"menu_name-6": {
		bg: "Гарнитури",
		en: "Garnishes",
		ru: "Гарниры",
	},
	"menu_name-7": {
		bg: "Десерти",
		en: "Desserts",
		ru: "Десерты",
	},
	
	"menu_name-8": {
		bg: "Предястия и Закуски",
		en: "Breakfasts and Snacks",
		ru: "Завтраки и Закуски",
	},
	"menu_descrip-1": {
		bg: "Истинско въплъщение на свежестта и вкуса на морето. От класически стриди и скариди на скара до гурме омар.",
		en: "A true embodiment of the freshness and taste of the sea. From classic oysters and grilled shrimp to gourmet lobster.",
		ru: "Настоящее воплощение свежести и вкуса моря. От классических устриц и гриль-креветок до изысканного омара.  ",
	},
	"menu_descrip-2": {
		bg: "Нашите ястия с месо и пиле са истински празник на вкуса за ценителите на месните деликатеси.",
		en: "Our meat and chicken dishes are a real festival of taste for connoisseurs of meat delicacies.",
		ru: "Наши блюда из мяса и курицы представляют собой настоящий фестиваль вкуса для ценителей мясных деликатесов.",
	},
	"menu_descrip-3": {
		bg: "Истинско въплъщение на италианската кулинарна традиция, където всяка чиния е изпълнена с богатство от вкусове и аромати.",
		en: "A true embodiment of the Italian culinary tradition, where every plate is filled with a wealth of flavors and aromas.",
		ru: "Истинное воплощение итальянской кулинарной традиции, где каждая тарелка наполнена богатством вкусов и ароматов.",
	},
	"menu_descrip-4": {
		bg: "Богат избор от свежи и вкусни комбинации, които ще задоволят и най-изисканите небца.",
		en: "A wide selection of fresh and delicious combinations that will satisfy even the most sophisticated palates.",
		ru: "Широкий выбор свежих и вкусных комбинаций, которые утоляют даже самый изысканный вкус.",
	},
	"menu_descrip-5": {
		bg: "Богат избор от класически и оригинални рецепти. Всяка пица се приготвя само от пресни и качествени продукти.",
		en: "Wide selection of classic and original recipes. Each pizza is prepared using only fresh and quality ingredients.",
		ru: "Широкий выбор классических и авторских рецептов. Каждая пицца приготовлена из свежих и качественных ингредиентов.",
	},
	"menu_descrip-6": {
		bg: "Предлагаме гарнитури като печени картофи, ориз и зеленчуци, които вървят добре с различни меса и риби.",
		en: "We offer side dishes such as baked potatoes, rice and vegetables that go well with a variety of meats and fish.",
		ru: "Мы предлагаем гарниры, такие как картофель, рис и овощи. Они отлично сочетаются с различными видами мяса и рыбы",
	},
	"menu_descrip-7": {
		bg: "Нашите десерти са приготвени с любов и внимание, за да завършат вашия обяд или вечеря с незабравима вкусна нотка.",
		en: "Our desserts are prepared with love and attention to complete your lunch or dinner with an unforgettable delicious touch.",
		ru: "Нашы десерты приготовлены с любовью и вниманием, чтобы завершить обед или ужин с незабываемым вкусным штрихом.",
	},
	
	"menu_descrip-8": {
		bg: "Предлагаме ви разнообразие от леки закуски и закуски като английска закуска, охлюви или фета сирене.",
		en: "We offer you a variety of snacks and breakfasts such as English breakfast, snails or feta cheese.",
		ru: "Мы предлагаем вам различные закуски и завтраки такие как английский завтрак, улитки или сыр фета. ",
	},
	"menu_btn-1": {
		bg: "МЕНЮ И ЦЕНИ",
		en: "MENU AND PRICES",
		ru: "МЕНЮ И ЦЕНЫ",
	},
	"menu_btn-2": {
		bg: "МЕНЮ И ЦЕНИ",
		en: "MENU AND PRICES",
		ru: "МЕНЮ И ЦЕНЫ",
	},
	"menu_btn-3": {
		bg: "МЕНЮ И ЦЕНИ",
		en: "MENU AND PRICES",
		ru: "МЕНЮ И ЦЕНЫ",
	},
	"menu_btn-4": {
		bg: "МЕНЮ И ЦЕНИ",
		en: "MENU AND PRICES",
		ru: "МЕНЮ И ЦЕНЫ",
	},
	"menu_btn-5": {
		bg: "МЕНЮ И ЦЕНИ",
		en: "MENU AND PRICES",
		ru: "МЕНЮ И ЦЕНЫ",
	},
	"menu_btn-6": {
		bg: "МЕНЮ И ЦЕНИ",
		en: "MENU AND PRICES",
		ru: "МЕНЮ И ЦЕНЫ",
	},
	"menu_btn-7": {
		bg: "МЕНЮ И ЦЕНИ",
		en: "MENU AND PRICES",
		ru: "МЕНЮ И ЦЕНЫ",
	},
	"menu_btn-8": {
		bg: "МЕНЮ И ЦЕНИ",
		en: "MENU AND PRICES",
		ru: "МЕНЮ И ЦЕНЫ",
	},
	"book-1": {
		bg: "РЕЗЕРВИРАЙТЕ МАСА",
		en: "BOOK THE TABLE",
		ru: "ЗАБРОНИРОВАТЬ СТОЛ",
	},
	"footer-1": {
		bg: "НАМЕРИ НИ",
		en: "HOW TO FIND US",
		ru: "КАК НАС НАЙТИ",
	},
	"hours-1": {
		bg: "Работни Часове",
		en: "Working Hours",
		ru: "Часы Работы",
	},
	"hours-2": {
		bg: "Пн - Пет:",
		en: "Mon - Fri:",
		ru: "Пн - Пт:",
	},
	"hours-3": {
		bg: "Сб:",
		en: "Sat:",
		ru: "Сб:",
	},
	"hours-4": {
		bg: "Неделя:",
		en: "Sun:",
		ru: "Вс:",
	},
	"socials-1": {
		bg: "Социална медия",
		en: "Socials",
		ru: "Социальные сети",
	},
	"contact-1": {
		bg: "Контакт",
		en: "Contact",
		ru: "Контакт",
	},
	"go_back": {
		bg: "< Върни се",
		en: "< Go Back",
		ru: "< Вернуться назад",
	},
	"fish_name": {
		bg: "Риба и Морски дарове",
		en: "Fish and Seafood",
		ru: "Рыба и Морепродукты",
	},
	"meat_name": {
		bg: "Месо и Пиле",
		en: "Meat and Chicken",
		ru: "Мясо и Курица",
	},
	"pasta_name": {
		bg: "Паста и Ризото",
		en: "Pasta and Rissotto",
		ru: "Паста и Ризотто",
	},
	"salads_name": {
		bg: "Салати",
		en: "Salads",
		ru: "Салаты",
	},
	"pizza_name": {
		bg: "Пица",
		en: "Pizza",
		ru: "Пицца",
	},
	"garnishes_name": {
		bg: "Гарнитури",
		en: "Garnishes",
		ru: "Гарниры",
	},
	"desserts_name": {
		bg: "Десерти",
		en: "Desserts",
		ru: "Десерты",
	},
	"more_name": {
		bg: "Предястия и Закуски",
		en: "Breakfasts and Snacks",
		ru: "Завтраки и Закуски",
	},
};

const anotherTexts = {
	

};


// Проверка пути страницы сайта
function checkPagePathName() {
	switch (currentPathName) {
		case "/index.html":
			currentTexts = homeTexts;
			break;

		case "menu/fish.html":
			currentTexts = homeTexts;
			break;

		case "menu/meat.html":
			currentTexts = homeTexts;
			break;

		case "menu/pasta.html":
			currentTexts = homeTexts;
			break;

		case "menu/salads.html":
			currentTexts = homeTexts;
			break;

		case "menu/pizza.html":
			currentTexts = homeTexts;
			break;

		case "menu/garnishes.html":
			currentTexts = homeTexts;
			break;

		case "menu/desserts.html":
			currentTexts = homeTexts;
			break;

		case "menu/more.html":
			currentTexts = homeTexts;
			break;

		default:
			currentTexts = homeTexts;
			break;
	}
}
checkPagePathName();

// Изменение языка у текстов
function changeLang() {
	for (const key in currentTexts) {
		let elem = document.querySelector(`[data-lang=${key}]`);
		if (elem) {
			elem.textContent = currentTexts[key][currentLang];
		}
	}
}
changeLang();

// Вешаем обработчики на каждую кнопку
langButtons.forEach((btn) => {
	btn.addEventListener("click", (event) => {
		if (!event.target.classList.contains("lang_btn_active,")) {
			currentLang = event.target.dataset.btn;
			localStorage.setItem("language", event.target.dataset.btn);
			resetActiveClass(langButtons, "lang_btn_active,");
			btn.classList.add("lang_btn_active,");
			changeLang();
		}
	});
});

// Сброс активного класса у переданного массива элементов
function resetActiveClass(arr, activeClass) {
	arr.forEach((elem) => {
		elem.classList.remove(activeClass);
	});
}



// Проверка языка браузера
function checkBrowserLang() {
	const navLang = navigator.language.slice(0, 2).toLowerCase();
	const result = allLangs.some((elem) => {
		return elem === navLang;
	});
	if (result) {
		return navLang;
	}
}

console.log("navigator.language", checkBrowserLang());
// non blocking map loader
window.addEventListener('load', function() {
    var iframe = document.createElement('iframe');
    iframe.src = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2932.39034337953!2d27.705652676558007!3d42.69545491384566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40a69e4da47460c1%3A0xceaf76861efd5d6f!2sRestaurant%20Djanny!5e0!3m2!1sru!2see!4v1715251050155!5m2!1sru!2see";
    iframe.allowFullscreen = true;
    iframe.loading = "lazy";
    iframe.referrerPolicy = "no-referrer-when-downgrade";
    iframe.className = "mapbox";
	iframe.title = "googlemap";
    document.getElementById('map-placeholder').appendChild(iframe);
});

// year loader
document.addEventListener('DOMContentLoaded', function() {
    // Get the current year
    const currentYear = new Date().getFullYear();
    
    // Find the element with the id 'current-year' and set its text content
    const yearElement = document.getElementById('current-year');
    if (yearElement) {
        yearElement.textContent = currentYear;
    }
});